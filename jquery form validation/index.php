<!DOCTYPE html>
<html>
<head>
	<title>Registration Form</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css">
	<style type="text/css">
		.form-control{
			border-radius: 0 !important;
			border:1px solid grey;
			box-shadow: 2px 3px 5px grey;
			color: blue !important;
			font-weight: 700 !important;
		}
		.gender{
			width: 20px;
			height: 20px;
		}
	</style>
</head>
<body>
	<div class="card bg-light" >
		<div class="container"><br><br>
			<h1 class="text-center text-warning">Registration Form</h1>
			<div class="col-lg-6 m-auto d-block">
				<form action="" method="post" onsubmit="return checkform()">
					<div class="form-group">
						<label for="user" class="text-danger font-weight-bold">Username*</label>
						<input type="text" name="username" id="username" placeholder="Enter Username" class="form-control" autocomplete="off">
						<p id="usercheck"></p>
					</div>
					<div class="form-group">
						<label for="user" class="text-danger font-weight-bold">Email Id*</label>
						<input type="text" name="email" id="email" placeholder="Enter Email Id" class="form-control" autocomplete="off">
						<p id="emailcheck"></p>
					</div>
					<div class="form-group">
						<label for="user" class="text-danger font-weight-bold">Phone no*.</label>
						<input type="text" name="phone" id="phone" placeholder="Enter Phone no." class="form-control" autocomplete="off">
						<p id="phonecheck"></p>
					</div>
					<div class="radio">
						<label for="user" class="text-danger font-weight-bold">Gender*:</label>

					</div>
					<div class="radio">
						<label><input type="radio" name="gender" id="gender" class="gender" checked=""> <strong>Female</strong></label>
						<label><input type="radio" name="gender" id="gender"  class="gender"> <strong>Male</strong></label>
					</div>
					<div class="form-group">
						<label for="user" class="text-danger font-weight-bold">Birth Date*</label>
						<input type="text" name="dob" id="dob" placeholder="Enter Birth Date" class="form-control bg-white" value="27-11-2019" readonly="" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="user" class="text-danger font-weight-bold">GSTIN*</label>
						<input type="text" name="gst" id="gst" placeholder="Enter GSTIN" class="form-control"autocomplete="off">
						<p id="gstcheck"></p>
					</div>
					<div class="form-group">
						<label for="user" class="text-danger font-weight-bold">PAN No*.</label>
						<input type="text" name="pan" id="pan" placeholder="Enter PAN Number" class="form-control"autocomplete="off">
						<p id="pancheck"></p>
					</div>

					<input type="submit" name="submit" id="submitbtn" class="btn btn-success">
				</form>

			</div>
			

		</div>

		<div></div>

		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

		<!-- Popper JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
		<script type="text/javascript">
			
			$('#usercheck').hide();
			$('#emailcheck').hide();
			$('#phonecheck').hide();
			$('#gstcheck').hide();
			$('#pancheck').hide();
			var name_err = true;
			var email_err = true;
			var phone_err = true;
			var gst_err = true;
			var pan_err = true;

			$('#username').keyup(function(){
				username_check();
			});

			function username_check(){
				var username_val =	$('#username').val();
				if (username_val =="" || username_val.length <=3  || username_val.length > 10) {
					$('#usercheck').show();
					$('#usercheck').html("**username length must be between 3 to 10");
					$('#usercheck').focus();
					$('#usercheck').css("color","red");
					name_err=false;
					return false;
				}
				else
				{
					$('#usercheck').hide();
				}
				var pattern = /^[a-zA-Z0-9]*$/ ;

				if (pattern.test(username_val) && username_val !=='') {
					$('#usercheck').hide();
				}
				else{
					$('#usercheck').show();
					$('#usercheck').html("**Name must include letters & numbers only");
					$('#usercheck').focus();
					$('#usercheck').css("color","red");
					name_err = false;
					return false;
				}
			}	

			$('#email').keyup(function(){
				email_check();
			});

			function email_check(){
				var email_val =	$('#email').val();
				if (email_val =="") {
					$('#emailcheck').show();
					$('#emailcheck').html("**Email is required");
					$('#emailcheck').focus();
					$('#emailcheck').css("color","red");
					email_err=false;
					return false;
				}
				else
				{
					$('#emailcheck').hide();
				}
				var emailreg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if (emailreg.test(email_val) && email_val !=='') {
					$('#emailcheck').hide();
				}
				else{
					$('#emailcheck').show();
					$('#emailcheck').html("correct form E.g, example@xyz.com");
					$('#emailcheck').focus();
					$('#emailcheck').css("color","red");
					email_err = false;
					return false;
				}

			}

			$('#phone').keyup(function(){
				phone_check();
			});

			function phone_check(){
				var phone_val =	$('#phone').val();
				if (phone_val =="" || phone_val.length!=10) {
					$('#phonecheck').show();
					$('#phonecheck').html("**phone No. must be of 10 digits");
					$('#phonecheck').focus();
					$('#phonecheck').css("color","red");
					phone_err=false;
					return false;
				}
				else
				{
					$('#phonecheck').hide();
				}
				var phonereg = /^[0-9-+]+$/;
				if (phonereg.test(phone_val) && phone_val !=='') {
					$('#phonecheck').hide();
				}
				else{
					$('#phonecheck').html("**Phone no. must not include characters");
					$('#phonecheck').show();
					$('#phonecheck').focus();
					$('#phonecheck').css("color","red");
					phone_err = false;
					return false;
				}
			}

			$('#gst').keyup(function(){
				gst_check();
			});

			function gst_check(){
				var gst_val =	$('#gst').val();
				if (gst_val =="" || gst_val.length!=15) {
					$('#gstcheck').show();
					$('#gstcheck').html("**GSTIN must be of 15 digits");
					$('#gstcheck').focus();
					$('#gstcheck').css("color","red");
					gst_err=false;
					return false;
				}
				else
				{
					$('#gstcheck').hide();
				}
				var gstreg = /^([0-9]){2}([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([0-9]){1}([a-zA-Z]){1}([0-9]){1}?$/;
				if (gstreg.test(gst_val) && gst_val !=='') {
					$('#gstcheck').hide();
				}
				else{
					$('#gstcheck').html("**GST Identification Number should be in '11AAAAA1111Z1A1' format");
					$('#gstcheck').show();
					$('#gstcheck').focus();
					$('#gstcheck').css("color","red");
					gst_err = false;
					return false;
				} 
			} 

			$('#pan').keyup(function(){
				pan_check();
			});

			function pan_check(){
				var pan_val =	$('#pan').val();
				if (pan_val =="" || pan_val.length!=10) {
					$('#pancheck').show();
					$('#pancheck').html("**PAN no. must be of 10 digits");
					$('#pancheck').focus();
					$('#pancheck').css("color","red");
					pan_err=false;
					return false;
				}
				else
				{
					$('#pancheck').hide();
				}
				var panreg = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/; 
				if (panreg.test(pan_val) && pan_val !=='') {
					$('#pancheck').hide();
				}
				else{
					$('#pancheck').html("**PAN Number should be in 'AAAPL1234C' format");
					$('#pancheck').show();
					$('#pancheck').focus();
					$('#pancheck').css("color","red");
					pan_err = false;
					return false;
				} 
			} 

			function checkform(){
				var name_err = username_check();
				var email_err = email_check();
				var phone_err = phone_check();
				var gst_err = gst_check();
				var pan_err = pan_check();

				if((name_err == false) || (email_err == false) ||(phone_err == false) || (gst_err == false) || (pan_err == false))
				{	
					alert('Please fill the form correctly.');
					return false;
				}
				else{
					alert('Form submitted successfully.');
					return false;
				}
			};

			$('#dob').datepicker({
				format:"dd-mm-YYYY",
				});
		</script>		
	</body>

	</html>
